import './App.css'
import Navbar from './assets/components/Navbar/Navbar'
import Carousel from './assets/components/Carousel/Carousel'
import Band from './assets/components/Band/Band'
import Tour from './assets/components/Tour/Tour'
import Contact from './assets/components/Contact/Contact'
import Footer from './assets/components/Footer/Footer'

function App() {
 

  return (
    <>
    <Navbar></Navbar>
    <Carousel></Carousel>
    <Band></Band>
    <Tour></Tour>
    <Contact></Contact>
    <Footer></Footer>
    </>
  
  )
}

export default App
