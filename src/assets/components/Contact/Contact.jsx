import "./Contact.css";

export default function Contact() {
  return (
    <>
      <div id="contact">
        <div className="container">
          <h2>Contact</h2>
          <p>We love our fans!</p>

          <div className="row">
            <div className="col-md-6">
              <ul className="contact-list">
                <li className="contact-list-item">
                  <img
                    src="https://www.pinclipart.com/picdir/middle/202-2020524_cliparto.png"
                    alt="Address"
                  />
                  Chicago, US
                </li>
                <li className="contact-list-item">
                  <img
                    src="https://w7.pngwing.com/pngs/279/613/png-transparent-mobile-phones-computer-icons-telephone-font-awesome-phone-miscellaneous-text-logo.png"
                    alt="Phone"
                  />
                  Phone: +00 1234567890
                </li>
                <li className="contact-list-item">
                  <img
                    src="https://banner2.cleanpng.com/20180718/vws/kisspng-email-logo-bounce-address-computer-icons-message-online-application-5b4f433eed6ac1.5937710415319212149725.jpg"
                    alt="Email"
                  />
                  Email: mail@mail.com
                </li>
              </ul>
            </div>

            <div className="col-md-6">
              <div className="contact-form">
                <form className="form-inline">
                  <div className="form-group col">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Name"
                    />
                  </div>
                  <div className="form-group col">
                    <input
                      type="email"
                      className="form-control"
                      placeholder="Email"
                    />
                  </div>
                  <div className="form-group col">
                    <textarea
                      className="form-control"
                      rows="4"
                      placeholder="Comment"
                    ></textarea>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
