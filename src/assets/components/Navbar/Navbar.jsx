import './Navbar.css';

export default function Navbar() {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a className="navbar-brand">Logo</a>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <a className="nav-link" href="#home">
                Home
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#band">
                Band
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#tour">
                Tour
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#contact">
                Contact
              </a>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                More
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <a className="dropdown-item" href="#">
                  More 1
                </a>
                <a className="dropdown-item" href="#">
                  More 2
                </a>
                <a className="dropdown-item" href="#">
                  Something else here
                </a>
              </div>
            </li>
            <li className="nav-item">
              <span className="nav-link">
                <img
                  src="https://image.pngaaa.com/270/8288270-middle.png"
                  alt="Search"
                  style={{ width: '20px', height: '20px' }} // Cambio della prop style in un oggetto JavaScript
                />
              </span>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
}
