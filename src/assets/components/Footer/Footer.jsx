import "./Footer.css";

export default function Footer() {
  return (
    <>
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <p>&copy; 04 marzo 2024, Simona Trifarò</p>
            </div>
            <div className="col-md-6 text-center">
              <p className="footer-arrow">
                <a href="#">&uarr;</a>
              </p>
            </div>
            <p className="footer-links">
              <a href="#">Work with Us</a> |<a href="#">Terms of Service</a> |
              <a href="#">Privacy Policy</a>
            </p>
          </div>
        </div>
      </footer>
    </>
  );
}
