import "./Band.css";

export default function Band() {
  return (
    <div id="band">
      <h2>THE BAND</h2>
      <span>We love music!</span>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla gravida
        nisi a ante ullamcorper, <br />
        sit amet convallis arcu aliquam. Integer et purus vel tortor auctor
        gravida. <br /> Lorem ipsum dolor sit amet consectetur adipisicing elit.
        Laborum expedita porro alias doloribus, dignissimos <br /> nam illum
        dolore consequuntur odit dolorum ad natus illo sit. Quis assumenda
        beatae voluptate repudiandae quam!
      </p>

      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">Name 1</h5>
                <img
                  src="https://live.staticflickr.com/6197/6028282566_499b202cfb.jpg"
                  alt="Member 1"
                  className="rounded-img"
                />
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">Name 2</h5>
                <img
                  src="https://www.quandlabriqueestbonne.fr/images/bricklife/biographie/lego-goldman-show.jpg"
                  alt="Member 2"
                  className="rounded-img"
                />
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">Name 3</h5>
                <img
                  src="https://s3-us-west-2.amazonaws.com/media.brothers-brick.com/2020/03/LEGO-Collectible-Minifigures-71027-CMF-Series-20-Review-U57I0-45.jpg"
                  alt="Member 3"
                  className="rounded-img"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
