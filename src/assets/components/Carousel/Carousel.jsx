import "./Carousel.css";

export default function Carousel() {
  return (
    <div id="carouselExampleCaptions" className="carousel slide">
      <div className="carousel-indicators">
        <button
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide-to="0"
          className="active"
          aria-current="true"
          aria-label="Slide 1"
        ></button>
        <button
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide-to="1"
          aria-label="Slide 2"
        ></button>
        <button
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide-to="2"
          aria-label="Slide 3"
        ></button>
        <button
          type="button"
          data-bs-target="#carouselExampleCaptions"
          data-bs-slide-to="3"
          aria-label="Slide 4"
        ></button>
      </div>
      <div className="carousel-inner">
        <div className="carousel-item active">
          <img
            src="https://images.musement.com/cover/0055/62/thumb_5461859_cover_header.jpeg"
            className="d-block w-100"
            alt="Barcellona"
          />
          <div className="carousel-caption d-none d-md-block">
            <h5>Barcellona</h5>
            <p>Barcellona lorem...</p>
          </div>
        </div>
        <div className="carousel-item">
          <img
            src="https://images.squarespace-cdn.com/content/v1/5448f34be4b078c86b41ae3a/1525870113802-PTRBXQX9ZT1DLPWY8Z3A/ke17ZwdGBToddI8pDm48kNeuZeoXUAxIDJZnwJZeFIZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0mhydAgiKdIfeAoxVgE7c7q3eeLOetEFAyUJ0GsXCFW-IeOHrWcAvr2kC-WVwPP5SA/shutterstock_313597526-River-Thames---Houses-of-Parliament-London-small.jpg"
            className="d-block w-100"
            alt="Londra"
          />
          <div className="carousel-caption d-none d-md-block">
            <h5>Londra</h5>
            <p>Londra lorem...</p>
          </div>
        </div>
        <div className="carousel-item">
          <img
            src="https://i.pinimg.com/originals/d0/e6/9a/d0e69a2654292f43a6e658bbc652111a.jpg"
            className="d-block w-100"
            alt="New York"
          />
          <div className="carousel-caption d-none d-md-block">
            <h5>New York</h5>
            <p>New York lorem...</p>
          </div>
        </div>
        <div className="carousel-item">
          <img
            src="https://wallpapercave.com/wp/J98wcP5.jpg"
            className="d-block w-100"
            alt="Rio de Janeiro"
          />
          <div className="carousel-caption d-none d-md-block">
            <h5>Rio de Janeiro</h5>
            <p>Rio de Janeiro lorem...</p>
          </div>
        </div>
      </div>
      <button
        className="carousel-control-prev"
        type="button"
        data-bs-target="#carouselExampleCaptions"
        data-bs-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button
        className="carousel-control-next"
        type="button"
        data-bs-target="#carouselExampleCaptions"
        data-bs-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  );
}
