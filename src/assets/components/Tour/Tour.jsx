import './Tour.css';

export default function Navbar() {
    return (
        <>
        <div id="tour">
    <h2>TOUR DATES</h2>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.!</p>
    <p>Remember to book your tickets!</p>

    <div className="tabella">
      <table className="table table-hover">
        <tbody>
          <tr>
            <td className="text-left">September <span className="red-box">Sold Out!</span></td>
          </tr>
          <tr>
            <td className="text-left">October <span className="red-box">Sold Out!</span></td>
          </tr>
          <tr>
            <td>November <span className="gray-dot">3</span></td>
          </tr>
          
        </tbody>
      </table>
    </div>

    <div className="cards">
      <div className="row justify-content-center">
        <div className="col-md-4">
          <div className="card">
            <img src="https://images.squarespace-cdn.com/content/v1/5448f34be4b078c86b41ae3a/1525870113802-PTRBXQX9ZT1DLPWY8Z3A/ke17ZwdGBToddI8pDm48kNeuZeoXUAxIDJZnwJZeFIZ7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0mhydAgiKdIfeAoxVgE7c7q3eeLOetEFAyUJ0GsXCFW-IeOHrWcAvr2kC-WVwPP5SA/shutterstock_313597526-River-Thames---Houses-of-Parliament-London-small.jpg" className="card-img-top" alt="Londra"/>
            <div className="card-body">
              <h5 className="card-title">Londra</h5>
              <p className="card-text">Friday 27 November 2015</p>
              <button className="btn btn-black">Buy Tickets</button>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className="card">
            <img src="https://images.musement.com/cover/0055/62/thumb_5461859_cover_header.jpeg" className="card-img-top" alt="Barcellona"/>
            <div className="card-body">
              <h5 className="card-title">Barcellona</h5>
              <p className="card-text">Saturday 28 November 2015</p>
              <button className="btn btn-black">Buy Tickets</button>
            </div>
          </div>
        </div>
        <div className="col-md-4">
          <div className="card">
            <img src="https://i.pinimg.com/originals/d0/e6/9a/d0e69a2654292f43a6e658bbc652111a.jpg" className="card-img-top" alt="..."/>
            <div className="card-body">
              <h5 className="card-title">New York</h5>
              <p className="card-text">Sunday 29 november 2015</p>
              <button className="btn btn-black">Buy Tickets</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
        </>
    );
}